const express = require('express');
const cassandra = require('cassandra-driver');
const path = require('path');
const async = require('async');
const bodyParser = require('body-parser');

const app = express();

const cassandraClient = new cassandra.Client({
    contactPoints: ['localhost'],
    keyspace: 'eleks'
});

const port = 8087;

const router = express.Router();

router.get('/api/getCarsData/:groupNumber', function(req, res) {
    const query = `select * from taxi_cars where group_number = ${req.params.groupNumber}`;
    cassandraClient.execute(query, function(err, result) {
        if (result) {
            let data = [];
            for (let i of result.rows)
                data.push(i);

            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json(data);
        }
    });
});

router.get('/api/getCarsData', function(req, res) {
    const query = 'select * from taxi_cars';
    cassandraClient.execute(query, function(err, result) {
        if (result) {
            let data = [];
            for (let i of result.rows)
                data.push(i);

            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json(data);
        }
    });
});


router.get('/api/getCarsInCentre/:time', function(req, res) {

    const query = `select * from in_centre where time = ${req.params.time}`;
    cassandraClient.execute(query, function (err, result) {
        if (result) {
            let data = [];
            for (let i of result.rows)
                data.push(i);

            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json(data);
        }
    });
    cassandraClient.execute('truncate table in_centre');
});

app.use(router);
app.listen(port);

// app.use(bodyParser.json() );       // to support JSON-encoded bodies
// app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
//     limit: '100mb',
//     extended: true
// }));
//
// app.use(express.static(__dirname + '/ui'));
//
//
// router.get('/', function(req, res) {
//     res.setHeader('Access-Control-Allow-Origin','*');
//     res.sendFile(path.join('/home/victor/Documents/WebstormProjects/nodejs_crud/www/index.html'));
// });

