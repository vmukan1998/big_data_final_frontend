let map;
let markers = [];
let markerCluster;
let previousGroupNumValue;

initDropDown();

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 49.842547, lng: 24.026544},
        zoom: 11
    });
}

setInterval(() => {
    $.when(getCarsPosition()).done((data) => updateMap(data));
}, 1000);

function updateMap(data) {
    $.when(getCarsInCentre(data[0]['time'], data[0]['group_number'])).done(data => alertCars(data));

    if (Number(previousGroupNumValue) !== Number(data[0]['group_number'])) {
        for (let i in markers)
            markers[i].setMap(null);

        markers = [];

        for (let i of data) {
            markers[`${i['car_number']} ${i['group_number']}`] = new google.maps.Marker({
                title: i['car_number'],
                position: {lat: i['latitude'], lng: i['longitude']},
                map: map
            });
        }

    } else {
        for (let i of data) {
            markers[`${i['car_number']} ${i['group_number']}`]
                .setPosition(new google.maps.LatLng(i['latitude'], i['longitude']));
        }
    }

    if (!markerCluster) {
        markerCluster = new MarkerClusterer(map, getMarkersArray(),
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }

    else {
        markerCluster.clearMarkers();

        markerCluster.addMarkers(getMarkersArray());
    }

    previousGroupNumValue = $('#my-dropdown').val();

}


function initDropDown() {
    for (let i = 1; i <= 40; i++) {
        let item = $('<option></option>').text(i).val(i);
        $('#my-dropdown').append(item);
    }

    $('#my-dropdown').append( $('<option></option>').text('Show all').val(-1));
}

function getMarkersArray() {
    markersArray = [];
    for (let i in markers)
        markersArray.push(markers[i]);

    return markersArray;
}

function getCarsPosition() {
    let req = 'http://localhost:8087/api/getCarsData';
    if ($('#my-dropdown').val() !== '-1')
        req +=`/${$('#my-dropdown').val()}`;

    return $.ajax({
        type: 'GET',
        url: req,
    });
}

function getCarsInCentre(time) {
    return $.ajax({
        type: 'GET',
        url: `http://localhost:8087/api/getCarsInCentre/${time}`,
    });
}

function alertCars(data, groupNumber){
    $('#alertingInCentre').text("")
    let str = "Cars in centre: ";
    for (let i of data) {
        str += i['car_number'];
	str += " " 
   }
    $('#alertingInCentre').text(str);
    console.log(data);
}
